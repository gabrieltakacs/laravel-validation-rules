# Laravel Validation Rules

Set of useful validation rules for Laravel project:
- Slovak Company ID (IČO)
- Slovak TAX ID (DIČ)
- Europe VAT ID (IČ DPH)
- Number (which can contain comma / point as decimal separator)

## Installation

1. Install composer package
```bash
composer require gabrieltakacs/laravel-validation-rules
```

2. Publish language files
```bash
php artisan vendor:publish --provider="GabrielTakacs\LaravelValidationRules\ServiceProvider"
```

## Usage

### Slovak Company ID
```php
use GabrielTakacs\Validation\Rule\CompanyId;

$request->validate(['company_id' => '12345678'], ['company_id' => new CompanyId()]); // Pass!
```

### Slovak TAX ID
```php
use GabrielTakacs\Validation\Rule\TaxId;

$request->validate(['tax_id' => '0123456789'], ['tax_id' => new TaxId()]); // Pass!
```

### EU VAT ID
```php
use GabrielTakacs\Validation\Rule\VatId;

$request->validate(['vat_id' => 'SK0123456789'], ['vat_id' => new VatId()]); // Pass!
```

### Number
This validator validates numeric attribute, which can contain comma / dot as decimal separator.

```php
use GabrielTakacs\Validation\Rule\Number;

$request->validate(['numeric_attribute' => '11.12'], ['numeric_attribute' => new Number()]); // Pass!
$request->validate(['numeric_attribute' => '1234,5678'], ['numeric_attribute' => new Number()]); // Pass!
$request->validate(['numeric_attribute' => '10'], ['numeric_attribute' => new Number()]); // Pass!
```
