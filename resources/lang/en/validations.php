<?php

return [
    'incorrect_company_id_format' => 'Entered company ID doesn\'t is invalid.',
    'incorrect_tax_id_format' => 'Entered TAX ID doesn\'t is invalid.',
    'incorrect_vat_id_format' => 'Entered VAT ID doesn\'t is invalid.',
    'incorrect_numeric_format' => 'Attribute is not a valid number.',
];
