<?php

return [
    'incorrect_company_id_format' => 'Zadané IČO nemá správny formát.',
    'incorrect_tax_id_format' => 'Zadané DIČ nemá správny formát.',
    'incorrect_vat_id_format' => 'Zadané IČ DPH nemá správny formát.',
    'incorrect_numeric_format' => 'Atribút nemá nespĺňa formát čísla.',
];
