<?php

namespace GabrielTakacs\LaravelValidationRules;

use GabrielTakacs\LaravelValidationRules\Validation\Rule\CompanyId;
use PHPUnit\Framework\TestCase;

class CompanyIdValidatorTest extends TestCase
{
    public function testValidSixDigitValue()
    {
        $companyIdValidator = new CompanyId();
        $result = $companyIdValidator->passes('', '123456');

        $this->assertTrue($result);
    }

    public function testValidEightDigitValue()
    {
        $companyIdValidator = new CompanyId();
        $result = $companyIdValidator->passes('', '12345679');

        $this->assertTrue($result);
    }

    public function testInvalidEightDigitValue()
    {
        $companyIdValidator = new CompanyId();
        $result = $companyIdValidator->passes('', '12345678');

        $this->assertFalse($result);
    }

    public function testShortValue()
    {
        $companyIdValidator = new CompanyId();
        $result = $companyIdValidator->passes('', '12345');

        $this->assertFalse($result);
    }

    public function testSevenDigitValue()
    {
        $companyIdValidator = new CompanyId();
        $result = $companyIdValidator->passes('', '1234567');

        $this->assertFalse($result);
    }

    public function testLongValue()
    {
        $companyIdValidator = new CompanyId();
        $result = $companyIdValidator->passes('', '123456789');

        $this->assertFalse($result);
    }
}
