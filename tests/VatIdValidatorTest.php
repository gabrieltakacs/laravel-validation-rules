<?php

namespace GabrielTakacs\LaravelValidationRules;

use GabrielTakacs\LaravelValidationRules\Validation\Rule\VatId;
use PHPUnit\Framework\TestCase;

class VatIdValidatorTest extends TestCase
{
    public function testValidSkValue()
    {
        $companyIdValidator = new VatId();
        $result = $companyIdValidator->passes('', 'SK1234567890');

        $this->assertTrue($result);
    }

    public function testLongSkValue()
    {
        $companyIdValidator = new VatId();
        $result = $companyIdValidator->passes('', 'SK12345678901');

        $this->assertFalse($result);
    }

    public function testShortSkValue()
    {
        $companyIdValidator = new VatId();
        $result = $companyIdValidator->passes('', 'SK123456789');

        $this->assertFalse($result);
    }
}
