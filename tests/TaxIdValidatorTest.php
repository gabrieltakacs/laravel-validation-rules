<?php

namespace GabrielTakacs\LaravelValidationRules;

use GabrielTakacs\LaravelValidationRules\Validation\Rule\TaxId;
use PHPUnit\Framework\TestCase;

class TaxIdValidatorTest extends TestCase
{
    public function testValidValue()
    {
        $validator = new TaxId();
        $result = $validator->passes('', '1234567890');

        $this->assertTrue((bool) $result);
    }

    public function testLongValue()
    {
        $validator = new TaxId();
        $result = $validator->passes('', '12345678901');

        $this->assertFalse((bool) $result);
    }

    public function testShortValue()
    {
        $validator = new TaxId();
        $result = $validator->passes('', '123456789');

        $this->assertFalse((bool) $result);
    }
}
