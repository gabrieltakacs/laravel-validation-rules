<?php

namespace GabrielTakacs\LaravelValidationRules\Validation\Rule;

use Ddeboer\Vatin\Validator;
use Illuminate\Contracts\Validation\Rule;

class VatId implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $validator = new Validator();

        return $validator->isValid($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return trans('laravel-validation-rules::validations.incorrect_vat_id_format');
    }
}
