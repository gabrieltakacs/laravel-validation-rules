<?php

namespace GabrielTakacs\LaravelValidationRules\Validation\Rule;

use Illuminate\Contracts\Validation\Rule;

class CompanyId implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Support for old 6-digits length Slovak company IDs
        if (preg_match('/^\d{6}$/', trim($value))) {
            return true;
        }

        // Newer Slovak company IDs consist of 8 digits (7 digits + 1 checksum digit)
        if (!preg_match('/^\d{8}$/', trim($value))) {
            return false;
        }

        $checkSum = 0;
        for ($i = 0; $i < 7; $i++) {
            $checkSum += $value[$i] * (8 - $i);
        }

        $rest = $checkSum % 11;
        if ($rest == 0) {
            $lastCipher = 1;
        } else if ($rest == 1) {
            $lastCipher = 0;
        } else {
            $lastCipher = 11 - $rest;
        }

        return (int) $value[7] === $lastCipher;
    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return trans('laravel-validation-rules::validations.incorrect_company_id_format');
    }
}
