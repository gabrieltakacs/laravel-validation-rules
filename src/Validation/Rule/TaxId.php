<?php

namespace GabrielTakacs\LaravelValidationRules\Validation\Rule;

use Illuminate\Contracts\Validation\Rule;

class TaxId implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('/^[\d]{10}$/', trim($value));
    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return trans('laravel-validation-rules::validations.incorrect_tax_id_format');
    }
}
