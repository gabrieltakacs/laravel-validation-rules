<?php

namespace GabrielTakacs\LaravelValidationRules;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang/', 'laravel-validation-rules');

        $this->publishes([
            __DIR__.'/../resources/lang/' => resource_path('lang/vendor/gabrieltakacs/laravel-validation-rules'),
        ]);
    }
}
