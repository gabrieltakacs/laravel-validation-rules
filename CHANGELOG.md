# Changelog

### 1.0.0
- Initial release including validation rules for SK Company ID (IČO), SK TAX ID (DIČ), EU VAT ID (IČ DPH).

### 1.0.1
- Added Laravel 6.x support

### 1.0.2
- Added Laravel 7.x support

### 1.0.3
- Added `number` validator to validate numbers with decimal point / comma

### 1.0.4
- Added Laravel 8.x support

### 1.0.5 
- Added php 8.x support
